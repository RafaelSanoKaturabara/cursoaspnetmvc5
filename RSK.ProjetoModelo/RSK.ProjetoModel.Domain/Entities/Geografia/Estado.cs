﻿using System;
using System.Collections.Generic;

namespace RSK.ProjetoModel.Domain.Entities.Geografia
{
    public class Estado
    {
        public Estado()
        {
            EstadoId = Guid.NewGuid();
        }
        public Guid EstadoId { get; set; }
        public string Nome { get; set; }
        public string UF { get; set; }
        // IEnumerable - Relacionamento de um para muitos
        public virtual IEnumerable<Cidade> Cidades { get; set; } // Propriedade virtual - Significa que ela é sobreescrevível (Ativa o lazy loading)

        // Lazy loading - Vem ativado -  Carregamento tardio. Exemplo do virtual - Não carrega do bando a lista de cidades. 
        //  Apenas consulta do banco no nomento que chama o Estado.Cidades
    }
}

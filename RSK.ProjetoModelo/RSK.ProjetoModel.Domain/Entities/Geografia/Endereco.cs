﻿using System;
using System.Collections.Generic;

namespace RSK.ProjetoModel.Domain.Entities.Geografia
{
    public class Endereco
    {
        public Endereco()
        {
            EnderecoId = Guid.NewGuid();
        }
        public Guid EnderecoId { get; set; }
        public string Rua { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string  CEP { get; set; }
        public Guid EstadoId { get; set; }
        public Guid CidadeId { get; set; }
        public virtual Cidade Cidade { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual ICollection<Cliente> ClienteList { get; set; } // Relacionamento de muitos para muitos por isso i ICollection
                                                                      // ICollection permito o .Add, IEnumerable não
        public virtual ICollection<Fornecedor> FornecedorList { get; set; }
    }
}

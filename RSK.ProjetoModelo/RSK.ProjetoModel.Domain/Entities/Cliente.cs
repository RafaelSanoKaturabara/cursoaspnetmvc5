﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSK.ProjetoModel.Domain.Entities.Geografia;
using RSK.ProjetoModel.Domain.Entities.Vendas;

namespace RSK.ProjetoModel.Domain.Entities
{
    public class Cliente
    {
        public Cliente()
        {
            ClienteId = Guid.NewGuid();
            EnderecoList = new List<Endereco>();
        }
        public Guid ClienteId { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }
        public virtual ICollection<Venda> ProdutosComprados { get; set; }
        public virtual ICollection<Endereco> EnderecoList { get; set; }

    }
}

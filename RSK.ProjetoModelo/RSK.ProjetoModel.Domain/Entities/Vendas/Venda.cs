﻿using System;
using System.Collections.Generic;

namespace RSK.ProjetoModel.Domain.Entities.Vendas
{
    public class Venda
    {
        public Venda()
        {
            VendaId = Guid.NewGuid();
            ProdutoList = new List<Produto>();
        }

        public Guid VendaId { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataCadastro { get; set; }
        public TipoVenda TipoVenda { get; set; }
        public ICollection<Produto> ProdutoList { get; set; }
        public Guid ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
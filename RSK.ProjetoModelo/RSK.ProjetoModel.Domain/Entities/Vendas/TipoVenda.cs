﻿namespace RSK.ProjetoModel.Domain.Entities.Vendas
{
    public enum TipoVenda
    {
        Vista = 1,
        Parcelado = 2,
        Credito = 3
    }
}
﻿using System;
using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Domain.Interfaces.Services
{
    public interface IClienteService : IDisposable
    {
        void AdicionarCliente(Cliente cliente);
    }
}
﻿using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository
{
    public interface IClienteRepository : IBaseRepository<Cliente>
    {
        Cliente ObterPorCPF(string cpf);
    }
}
﻿using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository
{
    public interface IFornecedorRepository : IBaseRepository<Fornecedor>
    {
        
    }
}
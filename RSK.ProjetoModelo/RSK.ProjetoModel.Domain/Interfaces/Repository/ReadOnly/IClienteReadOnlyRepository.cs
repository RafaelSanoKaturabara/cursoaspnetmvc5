﻿using System;
using System.Collections.Generic;
using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository.ReadOnly
{
    public interface IClienteReadOnlyRepository : IDisposable
    {
        Cliente GetById(Guid id);
        IEnumerable<Cliente> GetAll();
        Cliente ObterPorCPF(string cpf);
    }
}
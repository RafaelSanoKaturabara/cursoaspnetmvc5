﻿using RSK.ProjetoModel.Domain.Entities.Vendas;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository
{
    public interface IVendaRepository : IBaseRepository<Venda>
    {
        
    }
}
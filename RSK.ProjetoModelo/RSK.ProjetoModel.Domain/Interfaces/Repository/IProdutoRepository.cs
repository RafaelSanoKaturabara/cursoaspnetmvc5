﻿using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository
{
    public interface IProdutoRepository : IBaseRepository<Produto>
    {
        
    }
}
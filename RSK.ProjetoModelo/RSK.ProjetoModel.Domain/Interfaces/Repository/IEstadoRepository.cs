﻿using RSK.ProjetoModel.Domain.Entities.Geografia;

namespace RSK.ProjetoModel.Domain.Interfaces.Repository
{
    public interface IEstadoRepository : IBaseRepository<Estado>
    {
        
    }
}
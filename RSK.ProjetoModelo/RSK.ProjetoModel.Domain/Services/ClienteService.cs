﻿using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Domain.Interfaces.Repository;
using RSK.ProjetoModel.Domain.Interfaces.Services;

namespace RSK.ProjetoModel.Domain.Services
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _clienteRepository;

        public void AdicionarCliente(Cliente cliente)
        {
            // Verificar se a entidade está válida

            // Validar as regras de negocio para adicionar novo cliente

            // add cliente na base
            _clienteRepository.Add(cliente);
        }

        public void Dispose()
        {
        }
    }
}
﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Domain.Entities.Geografia;
using RSK.ProjetoModel.Domain.Entities.Vendas;
using RSK.ProjetoModel.Infra.Data.EntityConfig;

namespace RSK.ProjetoModel.Infra.Data.Context
{
    public class ProjetoModeloContext : DbContext
    {
        public ProjetoModeloContext()
            : base("ProjetoModeloContext")
        {
            
        }

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Cidade> Cidade { get; set; }
        public DbSet<Fornecedor> Fornecedor { get; set; }
        public DbSet<Venda> Venda { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>(); // Remove para parar de colocar o nome da tabela no plural
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); // Não deletar em cascata (um para muitos)
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); // Não deletar em cascata (muitos para muitos)

            modelBuilder.Properties()
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey()); // Sempre que uma propriedade tiver o final "Id" vai considerar chave primária

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar")); // Para o entity Framework criar varchar para os strings e não nvarchar que tem o dobro do tamanho

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new ClienteConfiguration());
            modelBuilder.Configurations.Add(new FornecedorConfiguration());
            modelBuilder.Configurations.Add(new ProdutoConfiguration());
            modelBuilder.Configurations.Add(new VendaConfiguration());
            modelBuilder.Configurations.Add(new EnderecoConfiguration());
            modelBuilder.Configurations.Add(new EstadoConfiguration());
            modelBuilder.Configurations.Add(new CidadeConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var dataCadastro = "DataCadastro";
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty(dataCadastro) != null))
            {
                if (entry.State == EntityState.Added)   // Foi adicionado?
                    entry.Property(dataCadastro).CurrentValue = DateTime.Now;

                if (entry.State == EntityState.Modified)    // Foi modificado?
                    entry.Property(dataCadastro).IsModified = false; // Para não alterar a data de cadastro em uma alteração
            }

            return base.SaveChanges();
        }
    }
}

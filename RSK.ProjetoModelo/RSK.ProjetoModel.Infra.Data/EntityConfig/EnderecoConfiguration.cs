﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities.Geografia;

namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class EnderecoConfiguration : EntityTypeConfiguration<Endereco>
    {
        public EnderecoConfiguration()
        {
            HasKey(e => e.EnderecoId);

            Property(e => e.Rua)
                .IsRequired()
                .HasMaxLength(150);

            Property(e => e.Numero)
                .IsRequired();

            Property(e => e.Bairro)
                .IsRequired()
                .HasMaxLength(50);

            Property(e => e.CEP)
                .IsRequired()
                .HasMaxLength(8);

            Property(e => e.Complemento)
                .IsRequired()
                .HasMaxLength(8);

            HasRequired(e => e.Estado)
                .WithMany()
                .HasForeignKey(e => e.EstadoId); // Chave estrangeira

            HasRequired(e => e.Cidade)
                .WithMany()
                .HasForeignKey(e => e.CidadeId); // Chave estrangeira
        }
    }
}
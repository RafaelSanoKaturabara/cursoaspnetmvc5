﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities.Geografia;

namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class EstadoConfiguration : EntityTypeConfiguration<Estado>
    {
        public EstadoConfiguration()
        {
            HasKey(e => e.EstadoId);

            Property(e => e.Nome)
                .IsRequired()
                .HasMaxLength(100);

            Property(e => e.UF)
                .IsRequired()
                .HasMaxLength(2);
        }
    }
}
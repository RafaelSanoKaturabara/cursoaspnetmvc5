﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities.Vendas;

namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class VendaConfiguration : EntityTypeConfiguration<Venda>
    {
        public VendaConfiguration()
        {
            HasKey(v => v.VendaId);

            Property(v => v.Valor)
                .IsRequired();

            Property(v => v.DataCadastro)
                .IsRequired();

            Property(v => v.TipoVenda)
                .IsRequired();

            HasMany(v => v.ProdutoList)
                .WithMany(p => p.VendaList)
                .Map(me =>
                {
                    me.MapLeftKey("VendaId");
                    me.MapRightKey("ProdutoId");
                    me.ToTable("VendaProduto");
                });
        }
    }
}
﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class FornecedorConfiguration : EntityTypeConfiguration<Fornecedor>
    {
        public FornecedorConfiguration()
        {
            Property(f => f.Nome)
                .IsRequired()
                .HasMaxLength(150);

            Property(f => f.Email)
                .IsRequired()
                .HasMaxLength(100);

            Property(f => f.CNPJ)
                .IsRequired()
                .HasMaxLength(14)
                .IsFixedLength();

            Property(f => f.Ativo)
                .IsRequired();

            Property(f => f.DataCadastro)
                .IsRequired();

            HasMany(f => f.EnderecoList)
                .WithMany(f => f.FornecedorList)
                .Map(me =>
                {
                    me.MapLeftKey("FornecedorId");
                    me.MapRightKey("EnderecoId");
                    me.ToTable("EnderecosFornecedor");
                });
        }
    }
}
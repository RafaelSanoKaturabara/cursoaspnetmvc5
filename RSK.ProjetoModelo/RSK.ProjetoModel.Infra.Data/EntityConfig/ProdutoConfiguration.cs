﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities;

namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class ProdutoConfiguration : EntityTypeConfiguration<Produto>
    {
        public ProdutoConfiguration()
        {
            HasKey(p => p.ProdutoId);

            Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(250);

            Property(p => p.Valor)
                .IsRequired();

            HasRequired(p => p.Fornecedor)
                .WithMany()
                .HasForeignKey(f => f.FornecedorId);
        }
    }
}
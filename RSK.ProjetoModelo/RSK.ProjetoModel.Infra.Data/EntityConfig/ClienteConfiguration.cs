﻿using System.Data.Entity.ModelConfiguration;
using RSK.ProjetoModel.Domain.Entities;
// Via Fluent API
namespace RSK.ProjetoModel.Infra.Data.EntityConfig
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(c => c.ClienteId);

            Property(c => c.Nome)
                .IsRequired()
                .HasMaxLength(150);

            Property(c => c.Email)
                .IsRequired();

            Property(c => c.CPF)
                .IsRequired()
                .HasMaxLength(11)
                .IsFixedLength();

            HasMany(c => c.EnderecoList)
                .WithMany(e => e.ClienteList)
                .Map(me =>
                {
                    me.MapLeftKey("ClienteId");
                    me.MapRightKey("EnderecoId");
                    me.ToTable("EnderecosCliente");
                });
        }
    }
}
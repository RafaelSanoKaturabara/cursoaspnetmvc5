﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using RSK.ProjetoModel.Domain.Interfaces.Repository;
using RSK.ProjetoModel.Infra.Data.Context;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class // TEntity foi definida e está dizendo que ela é uma class
    {
        protected IDbSet<TEntity> DbSet;        // Protected - para conseguir acessar quando herdar esta classe
        protected readonly ProjetoModeloContext DbContext;

        public BaseRepository()
        {
            DbContext = new ProjetoModeloContext(); 
            DbSet = DbContext.Set<TEntity>(); // Criando um dbSet com o tipo da entidade TEntity
        }
        public virtual void Add(TEntity obj)
        {
            DbSet.Add(obj);
            DbContext.SaveChanges();
        }

        public virtual void Dispose()
        {
            DbContext.Dispose();
            GC.SuppressFinalize(this);
        }

        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual TEntity GetById(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual void Remove(TEntity obj)
        {
            DbSet.Remove(obj);
            DbContext.SaveChanges();
        }

        public virtual void Update(TEntity obj)
        {
            var entry = DbContext.Entry(obj); // Criar uma entrada com o objeto do contexto
            DbSet.Attach(obj);  // atachar o obj no contexto
            entry.State = EntityState.Modified; // Informa que ele foi modificado
            DbContext.SaveChanges();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSK.ProjetoModel.Domain.Entities.Geografia;
using RSK.ProjetoModel.Domain.Interfaces.Repository;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    class CidadeRepository : BaseRepository<Cidade>, ICidadeRepository
    {
    }
}

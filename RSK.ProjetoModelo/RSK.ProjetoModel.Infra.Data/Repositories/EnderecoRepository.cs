﻿using RSK.ProjetoModel.Domain.Entities.Geografia;
using RSK.ProjetoModel.Domain.Interfaces.Repository;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    public class EnderecoRepository : BaseRepository<Endereco>, IEnderecoRepository
    {
        
    }
}
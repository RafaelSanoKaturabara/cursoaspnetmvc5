﻿using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Domain.Interfaces.Repository;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    public class FornecedorRepository : BaseRepository<Fornecedor>, IFornecedorRepository
    {
        
    }
}
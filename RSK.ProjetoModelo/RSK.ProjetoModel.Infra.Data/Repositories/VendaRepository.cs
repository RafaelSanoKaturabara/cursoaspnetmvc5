﻿using System.Data.Entity;
using RSK.ProjetoModel.Domain.Entities.Vendas;
using RSK.ProjetoModel.Domain.Interfaces.Repository;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    public class VendaRepository : BaseRepository<Venda>, IVendaRepository
    {
        public override void Add(Venda venda)
        {
            DbSet.Add(venda);
            foreach (var produto in venda.ProdutoList)
            {
                DbContext.Entry(produto).State = EntityState.Unchanged;
            }
            DbContext.SaveChanges();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Domain.Interfaces.Repository;

namespace RSK.ProjetoModel.Infra.Data.Repositories
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {
        public Cliente ObterPorCPF(string cpf)
        {
            return Find(c => c.CPF == cpf).FirstOrDefault();
            // ou
            //return DbSet.FirstOrDefault(c => c.CPF == cpf);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Domain.Interfaces.Repository.ReadOnly;

namespace RSK.ProjetoModel.Infra.Data.Repositories.ReadOnly
{
    public class ClienteReadOnlyRepository : BaseReadOnlyRepository, IClienteReadOnlyRepository
    {
        public Cliente GetById(Guid id)
        {
            using (IDbConnection cn = Connection) // utilizando o using não precisa fechar a conexão
            {
                var sql = @"SELECT * FROM Cliente WHERE ClienteId = '" + id + "'";

                cn.Open();
                var clientes = cn.Query<Cliente>(sql); // Dapper

                return clientes.First();
            }
        }

        public IEnumerable<Cliente> GetAll()
        {
            using (IDbConnection cn = Connection)
            {
                var sql = @"SELECT * FROM Cliente";

                cn.Open();
                var clientes = cn.Query<Cliente>(sql); // Dapper

                return clientes;
            }
        }

        public Cliente ObterPorCPF(string cpf)
        {
            using (IDbConnection cn = Connection)
            {
                var sql = @"SELECT * FROM Cliente WHERE CPF = '" + cpf + "'";

                cn.Open();
                var clientes = cn.Query<Cliente>(sql); // Dapper

                return clientes.First();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
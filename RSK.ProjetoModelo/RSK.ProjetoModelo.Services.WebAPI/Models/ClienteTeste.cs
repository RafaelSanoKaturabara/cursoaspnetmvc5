﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSK.ProjetoModelo.Services.WebAPI.Models
{
    public class ClienteTeste
    {

        public class Rootobject
        {
            public Class1[] Property1 { get; set; }
        }

        public class Class1
        {
            public string ClienteId { get; set; }
            public string Nome { get; set; }
            public string CPF { get; set; }
            public string Email { get; set; }
            public DateTime DataCadastro { get; set; }
            public bool Ativo { get; set; }
            public object ProdutosComprados { get; set; }
            public object[] EnderecoList { get; set; }
        }

    }
}
﻿using System;
using System.Web.Mvc;
using RSK.ProjetoModel.Domain.Entities;
using RSK.ProjetoModel.Infra.Data.Repositories;
using RSK.ProjetoModel.Infra.Data.Repositories.ReadOnly;

namespace RSK.ProjetoModel.Presentation.MVC.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ClienteReadOnlyRepository _clienteReadOnlyRepository = new ClienteReadOnlyRepository();
        private readonly ClienteRepository _clienteRepository = new ClienteRepository();
        // GET: Cliente
        public ActionResult Index()
        {
            return View(_clienteReadOnlyRepository.GetAll());
        }

        // GET: Cliente/Details/5
        public ActionResult Details(Guid id)
        {
            return View(_clienteReadOnlyRepository.GetById(id)); 
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Create(Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                _clienteRepository.Add(cliente);
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Cliente/Edit/5
        public ActionResult Edit(Guid id)
        {
            return View(_clienteReadOnlyRepository.GetById(id));
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View();
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
